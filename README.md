# Lista de Tareas Cool
# A sample Django TDD To Do app

# What do I need?
- Python 3.6+
- Docker
- Selenium WebDriver with Firefox

# How to run?
```
docker build . -t todo-tdd:latest
docker run todo-tdd:latest
```