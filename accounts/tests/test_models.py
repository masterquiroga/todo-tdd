# -*- coding: utf-8 -*-
from django.contrib.auth import get_user_model
from django.test import TestCase

from accounts.models import Token

User = get_user_model()


class UserModelTest(TestCase):
    def test_user_is_valid_with_email_only(self):
        user = User(email="hello@example.com")
        self.assertEqual(user.pk, "hello@example.com")
        user.full_clean()  # should not raise


class TokenModelTest(TestCase):
    def test_links_user_with_auto_generated_uid(self):
        token1 = Token.objects.create(email="hello@example.com")
        token2 = Token.objects.create(email="hello@example.com")
        self.assertNotEqual(token1.uid, token2.uid)
