# -*- coding: utf-8 -*-
from django.core.mail import send_mail
from django.shortcuts import redirect

# Create your views here.


def send_login_email(request):
    email = request.POST["email"]
    send_mail(
        "Tu login para la Lista de Tareas Cool",
        "Hola soy un contenido",
        "noreply@tareas.cool",
        [email],
    )
    return redirect("/")
