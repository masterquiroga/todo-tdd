Django==2.2.8
gunicorn==19.9.0
pre-commit==1.20.0
pytz==2019.3
selenium==3.141.0
