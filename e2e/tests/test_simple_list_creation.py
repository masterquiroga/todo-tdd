# -*- coding: utf-8 -*-
import os
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from .base import FunctionalTest


class NewVisitorTest(FunctionalTest):
    def test_can_start_a_list_and_retireve_it_later(self):
        # Cuando Ana visita el sitio web
        self.browser.get(self.live_server_url)
        # Entonces aparece el título
        self.assertIn("Tareas", self.browser.title)
        header_text = self.browser.find_element_by_tag_name("h1").text
        self.assertIn("Tareas", header_text)
        # Y ella pueda ingresar tareas
        inputbox = self.get_item_input_box()
        self.assertEqual(inputbox.get_attribute("placeholder"), "Ingresa una tarea")

        # Pero si ella ingresa una tarea
        inputbox.send_keys("ola")
        # Entonces la página se actualiza
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table("ola")

        # Pero si ella ingresa otra tarea
        inputbox = self.get_item_input_box()
        inputbox.send_keys("ke ase")
        inputbox.send_keys(Keys.ENTER)
        # Entonces deben aparecer las dos
        self.wait_for_row_in_list_table("ola")
        self.wait_for_row_in_list_table("ke ase")

    def test_multiple_users_can_start_lists_at_different_urls(self):
        # Cuando Beto hace una nueva lista
        self.browser.get(self.live_server_url)
        inputbox = self.get_item_input_box()
        inputbox.send_keys("Lorem ipsum")
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table("Lorem ipsum")
        # Entonces se le da una dirección única a su lista
        user2_list_url = self.browser.current_url
        self.assertRegex(user2_list_url, "/lists/.+")

        # Pero si Carmen llega
        self.browser.quit()
        self.browser = webdriver.Firefox()
        # Entonces Carmen no ve la lista de Beto
        self.browser.get(self.live_server_url)
        page_text = self.browser.find_element_by_tag_name("body").text
        self.assertNotIn("Lorem ipsum", page_text)
        self.assertNotIn("dolor sit amet", page_text)

        # Pero si Carmen hace una nueva lista
        inputbox = self.get_item_input_box()
        inputbox.send_keys("agua")
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table("agua")
        # Entonces se le da una dirección única a su lista distinta a la de Beto
        user3_list_url = self.browser.current_url
        self.assertRegex(user3_list_url, "/lists/.+")
        self.assertNotEqual(user2_list_url, user3_list_url)
        # Y las listas se muestran aparte
        page_text = self.browser.find_element_by_tag_name("body").text
        self.assertNotIn("Lorem ipsum", page_text)
        self.assertIn("milk", page_text)


if __name__ == "__main__":
    unittest.main(warnings=None)
