# -*- coding: utf-8 -*-
from unittest import skip

from django.utils.html import escape
from selenium.webdriver.common.keys import Keys

from .base import FunctionalTest
from lists.forms import DUPLICATE_ITEM_ERROR


class ItemValidationTest(FunctionalTest):
    def get_error_element(self):
        return self.browser.find_element_by_css_selector(".has-error")

    def test_cannot_add_empty_item(self):
        # Dado que Ana va al sitio con su lista

        # Cuando ella ingresa una tarea vacía
        self.browser.get(self.live_server_url)
        self.get_item_input_box().send_keys(Keys.ENTER)
        # Entonces la página se refreca mostrándole error de que no puede ser vacío
        self.wait_for(
            lambda: self.browser.find_element_by_css_selector("#id_text:invalid")
        )

        # Pero si ella ingresa una tarea no vacía
        inputbox = self.get_item_input_box()
        inputbox.send_keys("ola")
        inputbox.send_keys(Keys.ENTER)
        # Entonces la tarea debe aparecer
        self.wait_for_row_in_list_table("ola")

        # Pero si ella ingresa otra tarea
        inputbox = self.get_item_input_box()
        inputbox.send_keys("ke ase")
        inputbox.send_keys(Keys.ENTER)
        # Entonces deben aparecer los dos renglones de las tareas
        self.wait_for_row_in_list_table("ola")
        self.wait_for_row_in_list_table("ke ase")

        # User attempts the empty list item again, just to make sure
        # Pero si ella vuelve a ingresar una tarea vacía
        self.get_item_input_box().send_keys(Keys.ENTER)
        # Entonces la página se refreca mostrándole error de que no puede ser vacío
        self.wait_for(
            lambda: self.browser.find_element_by_css_selector("#id_text:invalid")
        )

    def test_cannot_add_duplicate_item(self):
        # Dado que Ana hace una nueva lista con un elemento
        self.browser.get(self.live_server_url)
        self.get_item_input_box().send_keys("Elemental mi querido Watson")
        self.get_item_input_box().send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table("1 Elemental mi querido Watson")

        # Cuando ella duplica el elemento
        self.get_item_input_box().send_keys("Elemental mi querido Watson")
        self.get_item_input_box().send_keys(Keys.ENTER)
        # Se le informa adecuadamente con un error útil
        self.wait_for(
            lambda: self.assertEqual(
                self.get_error_element().text, escape(DUPLICATE_ITEM_ERROR)
            )
        )

    def test_error_messages_are_cleared_on_input(self):
        # Dado que Ana tiene una lista con un elemento
        self.browser.get(self.live_server_url)
        self.get_item_input_box().send_keys("Vamo'a romperla")
        self.get_item_input_box().send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table("1 Vamo'a romperla")
        
        # Cuando ella duplica el elemento
        self.get_item_input_box().send_keys("Vamo'a romperla")
        self.get_item_input_box().send_keys(Keys.ENTER)
        # Entonces se le informa de error
        self.wait_for(lambda: self.assertTrue(self.get_error_element().is_displayed()))

        # Pero si pone otro elemento
        self.get_item_input_box().send_keys("aeiou")
        # Entonces el mensaje desaparece
        self.wait_for(lambda: self.assertFalse(self.get_error_element().is_displayed()))
