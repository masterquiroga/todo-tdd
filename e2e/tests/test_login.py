# -*- coding: utf-8 -*-
import re

from django.core import mail
from selenium.webdriver.common.keys import Keys

from .base import FunctionalTest

TEST_EMAIL = "user@example.com"
SUBJECT = "Tu login para la Lista de Tareas Cool"


class LoginTest(FunctionalTest):
    def test_can_get_email_link_to_log_in(self):
        # Dado que Ana visita el sitio para login
        # Cuando se inscribe a la plataforma
        self.browser.get(self.live_server_url)
        self.browser.find_element_by_name("email").send_keys(TEST_EMAIL)
        self.browser.find_element_by_name("email").send_keys(Keys.ENTER)
        # Entonces se le notifica que ya se envío un email
        self.wait_for(
            lambda: self.assertIn(
                "Checa tu email!", self.browser.find_element_by_tag_name("body").text
            )
        )
        email = mail.outbox[0]
        self.assertIn(TEST_EMAIL, email.to)
        self.assertEqual(email.subject, SUBJECT)

        # Pero cuando le llega el mail
        self.assertIn("Usa en enlace para ingresar:", email.body)
        url_search = re.search(r"http://.+/.+$", email.body)
        if not url_search:
            self.fail(f"Could not find url in email body:\n {email.body}")
        url = url_search.group(0)
        self.assertIn(self.live_server_url, url)
        # Y si ella le da click
        self.browser.get(url)
        # Entonces ella debería estar loggeada
        self.wait_for(lambda: self.browser.find_element_by_link_text("Log Out"))
        navbar = self.browser.find_element_by_css_selector(".navbar")
        self.assertIn(TEST_EMAIL, navbar.text)
