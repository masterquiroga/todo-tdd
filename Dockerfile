FROM ubuntu:16.04

# Deps
RUN apt-get update && apt-get install -y software-properties-common curl \
    && add-apt-repository ppa:ubuntugis/ubuntugis-unstable && apt-get update \
    && apt-get install -y python3-pip libssl-dev libffi-dev python3-gdal \
    && update-alternatives --install /usr/bin/python python /usr/bin/python3 10 \
    && update-alternatives --install /usr/bin/pip    pip    /usr/bin/pip3    10 \
    && rm -rf /var/lib/apt/lists/*
RUN apt-get update && apt-get install -y apt-transport-https wget
RUN sh -c 'echo "deb [arch=amd64] https://apt-mo.trafficmanager.net/repos/mssql-ubuntu-xenial-release/ xenial main" > /etc/apt/sources.list.d/mssqlpreview.list'
RUN apt-key adv --keyserver apt-mo.trafficmanager.net --recv-keys 417A0893
RUN apt-get update -y
RUN apt-get install -y libodbc1-utf16 unixodbc-utf16 unixodbc-dev-utf16
RUN ACCEPT_EULA=Y apt-get install -y msodbcsql

# Locales
RUN apt-get install -y locales
RUN echo "es_MX.UTF-8 UTF-8" > /etc/locale.gen
RUN locale-gen
RUN apt-get remove -y curl

# Django
RUN mkdir /code
WORKDIR /code
ADD . /code/
RUN ls /code/
RUN pip install pip pipenv --upgrade
RUN pip install -r /code/requirements.txt
RUN python /code/manage.py collectstatic --noinput
RUN python /code/manage.py makemigrations
RUN python /code/manage.py migrate
EXPOSE 8080
WORKDIR /code/source
ENTRYPOINT ["python", "/code/manage.py", "runserver", "0.0.0.0:8080"]
